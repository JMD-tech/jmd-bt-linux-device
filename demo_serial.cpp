
/*  NOT YET WORKING... */


//#include <stdio.h>
//#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string>
#include <iostream>
//#include <fstream>
//#include <cstdint>

//#include <sys/un.h>
//#include <sys/stat.h>

#include <sys/socket.h>
#include <bluetooth/bluetooth.h>
//#include <bluetooth/l2cap.h>
#include <bluetooth/rfcomm.h>

#include "jmd-bt-linux-device.hpp"

using std::string; using std::cout; using std::endl; using std::max;



bool server_read_data(int in_fd)
{
	char buf[1024] = { 0 };
	int bytes_read;
	int opts = 0;

	int sock_fd = in_fd;
	
	printf("server_read_data called\n");

	// set socket for blocking IO
	fcntl(sock_fd, F_SETFL, opts);
	opts = fcntl(sock_fd, F_GETFL);
	if (opts < 0)
	{
		perror("fcntl(F_GETFL)");
		exit(EXIT_FAILURE);
	}

	opts &= ~O_NONBLOCK;
	
	if (fcntl(sock_fd, F_SETFL,opts) < 0)
	{
		perror("fcntl(F_SETFL)");
		exit(EXIT_FAILURE);
	}

	// read data from the client
	bytes_read = read(sock_fd, buf, sizeof(buf));
	if ( bytes_read > 0 )
		printf("received [%s]\n", buf);
	else
		printf("error reading from client [%d] %s\n", errno, strerror(errno));

	// close connection
	close(sock_fd);

	return false;
}



int main( int argc, char **argv )
{
	bool debug = true;
	//string svname = "JMDserial";
	string svname = "com.jmd-tech.btdevice-demo";

	bt_initsequence("RasPiSER",0x002114);

	auto dbus_CX = BlueZ_init( svname );
	cout << "Calling RegisterProfile" << endl;
	jdbus_RegisterProfile_serial( dbus_CX );

	cout << "Now enabling bluetooth sockets" << endl;

	//int scnt = socket(AF_BLUETOOTH, SOCK_SEQPACKET, BTPROTO_L2CAP),
	//	sint = socket(AF_BLUETOOTH, SOCK_SEQPACKET, BTPROTO_L2CAP);

	int s = socket(AF_BLUETOOTH, SOCK_STREAM, BTPROTO_RFCOMM);

	int enable = 1;
	//if (setsockopt(scnt, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0) perror("scnt: setsockopt(SO_REUSEADDR) failed");
	//if (setsockopt(sint, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0) perror("sint: setsockopt(SO_REUSEADDR) failed");

	// NONBLOCK AS SECURITY FOR select() -> accept() (race condition of connection dropped between)
	//fcntl(scnt, F_SETFL, O_NONBLOCK);
	//fcntl(sint, F_SETFL, O_NONBLOCK);
	//fcntl(s, F_SETFL, O_NONBLOCK);

	// Nice workaround for https://stackoverflow.com/questions/9751710/c-c-warning-address-of-temporary-with-bdaddr-any-bluetooth-library
	bdaddr_t bdaddr_any =  {{0, 0, 0, 0, 0, 0}};

	struct sockaddr_rc cnt_addr = {0}, rem_addr = {0};
	socklen_t opt = sizeof(rem_addr);

	// bind socket to port 1 of the first available local bluetooth adapter
	cnt_addr.rc_family = AF_BLUETOOTH;
	cnt_addr.rc_bdaddr = bdaddr_any;
	cnt_addr.rc_channel = (uint8_t) 1;
	if (bind(s,(struct sockaddr *)&cnt_addr, sizeof(cnt_addr))) perror("cnt: bind failed.");

	// Some other code put the backlog to 1
	int bklog = 5;
	if (listen(s,bklog)) perror("int: listen failed");
	
	int ccli=0,icli=0;	//TODO: define exact behaviour with multiple connections.
	/* proposals:
	For HID or RFcomm device: backlog = 2, new connection kills previous one (optionally only from already paired peers) (optionally only if >IDLE sec)
	For Audio sink: backlog = ~10, remember each paired device. Then choose to either allow mixing, or one at a time output.
		Ideally we should use a bluetooth "ping" (or is there an integrated function to detect connection closed/dropped)
	*/

	fd_set set;
	struct timeval timeout;

	// accept one connection
	int client = accept(s, (struct sockaddr *)&rem_addr, &opt);

	char buf[1024] = { 0 };
	//ba_to_str( &rem_addr.rc_bdaddr, buf );
	strcpy(buf,batostr(&rem_addr.rc_bdaddr));
	fprintf(stderr, "accepted connection from %s\n", buf);
	memset(buf, 0, sizeof(buf));

	// read data from the client
	int bytes_read = read(client, buf, sizeof(buf));
	if( bytes_read > 0 ) {
		printf("received [%s]\n", buf);
	}

	// close connection
	close(client);
	close(s);

/*
	int nfds = max(scnt,sint);
	++nfds;

	cout << "Entering main loop." << endl;

	while (1)
	{
		FD_ZERO(&set);
		//TODO: add descriptors to ccli and icli? (detect disconnect?)
		FD_SET(scnt, &set); FD_SET(sint, &set);
		timeout.tv_sec = 10; timeout.tv_usec = 0;
		int rv = select(nfds, &set, NULL, NULL, &timeout);
		if (rv) cout << "select() = " << rv << endl;
		if (rv > 0)
		{
			if (FD_ISSET(scnt,&set))
			{
				if (debug) cout << "Control channel awakened" << endl;
				opt=sizeof(sockaddr_l2);
				ccli = accept(scnt, (struct sockaddr *)&remc_addr, &opt);
				if (ccli) 
				{
					char buf[20]={0}; ba2str( &remc_addr.l2_bdaddr, buf );
					cout << "Connection on control channel from " << buf << endl;
				}
			}
			if (FD_ISSET(sint,&set))
			{
				if (debug) cout << "Interrupt channel awakened" << endl;
				opt=sizeof(sockaddr_l2);
				icli = accept(sint, (struct sockaddr *)&remi_addr, &opt);
				if (icli)
				{
					char buf[20]={0}; ba2str( &remc_addr.l2_bdaddr, buf );
					cout << "Connection on interrupt channel from " << buf << endl;
				}
			}

		}

		if (icli)
		{
			// Call server_read_data
		}

	}
	*/

	return 0;
}


