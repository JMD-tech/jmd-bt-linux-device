#ifndef __BLUEZ_DEVLIB_HPP__
#define __BLUEZ_DEVLIB_HPP__

#define BLUEZ_DEVLIB_WANT_DBUS_INTERNALS

#include <string>

#ifdef BLUEZ_DEVLIB_WANT_DBUS_INTERNALS
	#include <dbus/dbus.h>

	void jdbus_array_add_dict( DBusMessageIter *it_array, const char *keyname, const char *valtypesign, int type, const void *value );
	void jdbus_array_add_bool( DBusMessageIter *it_array, const std::string& keyname, bool value );
	void jdbus_array_add_uint16( DBusMessageIter *it_array, const std::string& keyname, uint16_t value );
	void jdbus_array_add_string( DBusMessageIter *it_array, const char *keyname, const char *value );
	void jdbus_array_add_string( DBusMessageIter *it_array, const std::string& keyname, const std::string& value );
	uint32_t jdbus_RequestName( DBusConnection *dbus_CX, const std::string& service_name );
	std::string jdbus_GetNameOwner( DBusConnection *dbus_CX, const std::string& service_name );
	std::string jdbus_Introspect( DBusConnection *dbus_CX, const std::string& service_name, const std::string& path );

#else
#endif

void jdbus_RegisterProfile( DBusConnection *dbus_CX, const std::string& service_record );
void jdbus_RegisterProfile_serial( DBusConnection *dbus_CX );
DBusConnection *BlueZ_init( const std::string& svname );

bool bt_initsequence( const std::string &name, uint32_t btclass );	// Returns true if ok, false on fail.
bool bt_discoverable( bool disco=true );
int udgsock_create( const char* sockname );


#endif
