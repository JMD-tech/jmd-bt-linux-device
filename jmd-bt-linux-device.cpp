#include "jmd-bt-linux-device.hpp"

#include <iostream>
#include <fstream>
#include <cstring>
#include <unistd.h>
#include <dbus/dbus.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/stat.h>

//#include "jmd-utils.hpp"	// temporary for tests

using std::string; using std::cout; using std::endl;

static void check_and_abort(DBusError *error) {
	if (!dbus_error_is_set(error)) return;
	puts(error->message);
	abort();
}

void jdbus_array_add_dict( DBusMessageIter *it_array, const char *keyname, const char *valtypesign, int type, const void *value )
{
	DBusMessageIter it_entry, it_value;
	dbus_message_iter_open_container(it_array, DBUS_TYPE_DICT_ENTRY, NULL, &it_entry);
	dbus_message_iter_append_basic(&it_entry, DBUS_TYPE_STRING, &keyname);
	dbus_message_iter_open_container(&it_entry, DBUS_TYPE_VARIANT, valtypesign, &it_value);
	dbus_message_iter_append_basic(&it_value, type, value);
	dbus_message_iter_close_container(&it_entry, &it_value);
	dbus_message_iter_close_container(it_array, &it_entry);
}

void jdbus_array_add_bool( DBusMessageIter *it_array, const string& keyname, bool value )
{
	dbus_bool_t bval=value?1:0;
	jdbus_array_add_dict( it_array, keyname.c_str(), DBUS_TYPE_BOOLEAN_AS_STRING, DBUS_TYPE_BOOLEAN, &bval );
}

void jdbus_array_add_uint16( DBusMessageIter *it_array, const string& keyname, uint16_t value )
{
	dbus_uint16_t val=value;
	jdbus_array_add_dict( it_array, keyname.c_str(), DBUS_TYPE_UINT16_AS_STRING, DBUS_TYPE_UINT16, &val );
}

void jdbus_array_add_string( DBusMessageIter *it_array, const char *keyname, const char *value )
{
	jdbus_array_add_dict( it_array, keyname, DBUS_TYPE_STRING_AS_STRING, DBUS_TYPE_STRING, &value );
}

void jdbus_array_add_string( DBusMessageIter *it_array, const string& keyname, const string& value )
{
	jdbus_array_add_string( it_array, keyname.c_str(), value.c_str() );
}

uint32_t jdbus_RequestName( DBusConnection *dbus_CX, const string& service_name )
{
	char *svname = strdup( service_name.c_str() );
	uint32_t uintVal=0, flags=0;
	DBusError dbus_err;
	DBusMessage *msgQuery = dbus_message_new_method_call("org.freedesktop.DBus", "/org/freedesktop/DBus", "org.freedesktop.DBus", "RequestName");
	dbus_message_append_args( msgQuery, DBUS_TYPE_STRING, &svname, DBUS_TYPE_UINT32, &flags, DBUS_TYPE_INVALID);

	dbus_error_init(&dbus_err);	
	DBusMessage *msgReply = dbus_connection_send_with_reply_and_block(dbus_CX, msgQuery, 1000, &dbus_err);
	check_and_abort(&dbus_err);

	dbus_message_get_args(msgReply, &dbus_err, DBUS_TYPE_UINT32, &uintVal, DBUS_TYPE_INVALID);
	check_and_abort(&dbus_err);
	
	dbus_message_unref(msgReply);
	dbus_message_unref(msgQuery);
	dbus_error_free(&dbus_err);
	free(svname);

	return uintVal;
}

string jdbus_GetNameOwner( DBusConnection *dbus_CX, const string& service_name )
{
	const char *stringVal = NULL;
	char *svname = strdup( service_name.c_str() );
	DBusError dbus_err;
	DBusMessage *msgQuery = dbus_message_new_method_call("org.freedesktop.DBus", "/org/freedesktop/DBus", "org.freedesktop.DBus", "GetNameOwner");
	dbus_message_append_args( msgQuery, DBUS_TYPE_STRING, &svname, DBUS_TYPE_INVALID);

	dbus_error_init(&dbus_err);	
	DBusMessage *msgReply = dbus_connection_send_with_reply_and_block(dbus_CX, msgQuery, 1000, &dbus_err);
	check_and_abort(&dbus_err);

    dbus_message_get_args(msgReply, &dbus_err, DBUS_TYPE_STRING, &stringVal, DBUS_TYPE_INVALID);
	check_and_abort(&dbus_err);

	string resu = stringVal;

	dbus_message_unref(msgReply);
	dbus_message_unref(msgQuery);
	dbus_error_free(&dbus_err);
	free(svname);
	return resu;
}

string jdbus_Introspect( DBusConnection *dbus_CX, const string& service_name, const string& path )
{
	const char *stringVal = NULL;
	DBusError dbus_err;
	DBusMessage *msgQuery = dbus_message_new_method_call(service_name.c_str(), path.c_str(), "org.freedesktop.DBus.Introspectable", "Introspect");
	// no parameters for this one

	dbus_error_init(&dbus_err);	
	DBusMessage *msgReply = dbus_connection_send_with_reply_and_block(dbus_CX, msgQuery, 1000, &dbus_err);
	check_and_abort(&dbus_err);

	dbus_message_get_args(msgReply, &dbus_err, DBUS_TYPE_STRING, &stringVal, DBUS_TYPE_INVALID);
	check_and_abort(&dbus_err);

	string resu = stringVal;

	dbus_message_unref(msgReply);
	dbus_message_unref(msgQuery);
	dbus_error_free(&dbus_err);
	return resu;
}

//TODO: pass UUID and device (optionnal)
void jdbus_RegisterProfile( DBusConnection *dbus_CX, const string& service_record )
{
	// First get NameOwner
	string dest = jdbus_GetNameOwner(dbus_CX,"org.bluez");
    cout << "Owner for " << "org.bluez" << " is: " << dest << endl;	 // :1.5 as writing this test

	DBusError dbus_err;
	DBusMessage *msgQuery = dbus_message_new_method_call(dest.c_str(), "/org/bluez", "org.bluez.ProfileManager1", "RegisterProfile");

	const char *obj="/org/bluez/hci0",*uidsomething="00001124-0000-1000-8000-00805f9b34fb";
	dbus_message_append_args( msgQuery, DBUS_TYPE_OBJECT_PATH, &obj, DBUS_TYPE_INVALID);
	dbus_message_append_args( msgQuery, DBUS_TYPE_STRING, &uidsomething, DBUS_TYPE_INVALID);
	
	DBusMessageIter it_init, it_array;

    dbus_message_iter_init_append(msgQuery, &it_init);
    dbus_message_iter_open_container(&it_init, DBUS_TYPE_ARRAY,
			DBUS_DICT_ENTRY_BEGIN_CHAR_AS_STRING
			DBUS_TYPE_STRING_AS_STRING DBUS_TYPE_VARIANT_AS_STRING
			DBUS_DICT_ENTRY_END_CHAR_AS_STRING, 
				&it_array);

		jdbus_array_add_bool( &it_array, "AutoConnect", true );
		jdbus_array_add_string( &it_array, "ServiceRecord", service_record );
    
    dbus_message_iter_close_container(&it_init, &it_array);

	dbus_error_init(&dbus_err);	
	DBusMessage *msgReply = dbus_connection_send_with_reply_and_block(dbus_CX, msgQuery, 1000, &dbus_err);
	check_and_abort(&dbus_err);
	dbus_message_unref(msgReply);
	dbus_message_unref(msgQuery);
	dbus_error_free(&dbus_err);

	//TODO: documentation to at least check success, when we'll do a non-abort version
	// https://github.com/pauloborges/bluez/blob/master/doc/profile-api.txt
	// 2 possible errors: org.bluez.Error.InvalidArguments and org.bluez.Error.AlreadyExists
	// Only the latter can be caused by the calling application. (Maybe unless a screwed service_record counts as an invalid argument)
}

void jdbus_RegisterProfile_serial( DBusConnection *dbus_CX )
{
	// First get NameOwner
	string dest = jdbus_GetNameOwner(dbus_CX,"org.bluez");
    cout << "Owner for " << "org.bluez" << " is: " << dest << endl;	 // :1.5 as writing this test

	DBusError dbus_err;
	DBusMessage *msgQuery = dbus_message_new_method_call(dest.c_str(), "/org/bluez", "org.bluez.ProfileManager1", "RegisterProfile");

	const char *obj="/bluetooth/profile/serial_port",*uidsomething="00001101-0000-1000-8000-00805f9b34fb";
	dbus_message_append_args( msgQuery, DBUS_TYPE_OBJECT_PATH, &obj, DBUS_TYPE_INVALID);
	dbus_message_append_args( msgQuery, DBUS_TYPE_STRING, &uidsomething, DBUS_TYPE_INVALID);
	
	DBusMessageIter it_init, it_array;

    dbus_message_iter_init_append(msgQuery, &it_init);
    dbus_message_iter_open_container(&it_init, DBUS_TYPE_ARRAY,
			DBUS_DICT_ENTRY_BEGIN_CHAR_AS_STRING
			DBUS_TYPE_STRING_AS_STRING DBUS_TYPE_VARIANT_AS_STRING
			DBUS_DICT_ENTRY_END_CHAR_AS_STRING, 
				&it_array);

		//jdbus_array_add_bool( &it_array, "AutoConnect", true );
		//jdbus_array_add_string( &it_array, "ServiceRecord", service_record );

		//TODO: Channel
		jdbus_array_add_uint16( &it_array, "Channel", 22 );
		jdbus_array_add_string( &it_array, "Service", uidsomething );
		jdbus_array_add_string( &it_array, "Name", "Serial Port" );
		jdbus_array_add_string( &it_array, "Role", "server" );

    
    dbus_message_iter_close_container(&it_init, &it_array);

	dbus_error_init(&dbus_err);	
	DBusMessage *msgReply = dbus_connection_send_with_reply_and_block(dbus_CX, msgQuery, 1000, &dbus_err);
	check_and_abort(&dbus_err);
	dbus_message_unref(msgReply);
	dbus_message_unref(msgQuery);
	dbus_error_free(&dbus_err);

	//TODO: documentation to at least check success, when we'll do a non-abort version
	// https://github.com/pauloborges/bluez/blob/master/doc/profile-api.txt
	// 2 possible errors: org.bluez.Error.InvalidArguments and org.bluez.Error.AlreadyExists
	// Only the latter can be caused by the calling application. (Maybe unless a screwed service_record counts as an invalid argument)
}

//TODO: svname parameter
// or might we close it after this init sequence?
DBusConnection *BlueZ_init( const string& svname )
{
	DBusConnection *dbus_CX = NULL;
	DBusError dbus_err;

	dbus_error_init(&dbus_err);
	dbus_CX = dbus_bus_get(DBUS_BUS_SYSTEM, &dbus_err);
	check_and_abort(&dbus_err);

	// RequestName
	//string svname = "org.thanhle.btkbservice";
	uint32_t ren = jdbus_RequestName(dbus_CX,svname);
	cout << "Response for RequestName " << svname << " is: " << ren << endl;

	return dbus_CX;
}

bool bt_initsequence( const std::string &name, uint32_t btclass )
{
	//system("hciconfig -a");
	char tmp[10];
	sprintf(tmp,"0x%06X",btclass);
	string cmd="hciconfig hci0 up class "+string(tmp)+" name \""+name+"\"";
	if (system(cmd.c_str())) return false;
	system("systemctl start bt-agent-mini.service");
	return true;
}

bool bt_discoverable( bool disco )
{
	string cmd="hciconfig hci0 "+string(disco?"piscan":"pscan");
	if (system(cmd.c_str())) return false;
	return true;
}

int udgsock_create( const char* sockname )
{
	unlink(sockname); // Security just in case we weren't shut cleanly

	struct sockaddr_un addr;
	addr.sun_family = AF_LOCAL;
	strcpy(addr.sun_path, sockname);
	socklen_t addrlen = SUN_LEN(&addr);
	int sock_fd = socket(PF_LOCAL, SOCK_DGRAM, 0);
	if (sock_fd < 0) return sock_fd;
		//abandon("Server socket creation");
	int b_ret;
	if ( (b_ret=bind(sock_fd, (struct sockaddr *) &addr, addrlen)) < 0) return b_ret;	// TODO: better error return.
		//abandon("Server socket binding");

	chmod(sockname, S_IRWXU | S_IRWXG | S_IRWXO);
	return sock_fd;
}

