
CXXFLAGS+=$(shell pkg-config --cflags dbus-1) 

LDFLAGS+=$(shell pkg-config --libs dbus-1) -lbluetooth

#TODO: have this by default, but allow overriding
PREFIX=/usr/local

OBJS=jmd-bt-linux-device.o

default: all

all: staticlib demo_serial demo_keyboard

clean:
	rm -f *.o *.a *.so demo_keyboard demo_serial

demo_serial: demo_serial.o $(OBJS)
	$(CXX) $(CXXFLAGS) -o demo_serial demo_serial.o $(OBJS) $(LDFLAGS)

demo_keyboard: demo_keyboard.o $(OBJS)
	$(CXX) $(CXXFLAGS) -o demo_keyboard demo_keyboard.o $(OBJS) $(LDFLAGS)

staticlib: jmd-bt-linux-device.o
	ar -rcs libjmd-bt-linux-device.a jmd-bt-linux-device.o

install: staticlib
	install -d $(DESTDIR)$(PREFIX)/lib/
	install -m 644 libjmd-bt-linux-device.a $(DESTDIR)$(PREFIX)/lib/
	install -d $(DESTDIR)$(PREFIX)/include/
	install -m 644 jmd-bt-linux-device.hpp $(DESTDIR)$(PREFIX)/include/

