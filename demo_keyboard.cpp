
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstdint>

#include <bluetooth/bluetooth.h>
#include <bluetooth/l2cap.h>

#include "jmd-bt-linux-device.hpp"
//#include "jmd-utils.hpp"

//#include "pipecommand.hpp"

using std::string; using std::cout; using std::endl; using std::max;

bool debug = true;

/*
	Boot sequence for Bluetooth:
	-(First of all, we should have disabled pscan, before anything, maybe even have the hci interface by default off at Linux boot until we start it)
	-First, we setup Name, class etc of the device (hciconfig)
	-Then we start bt-agent-mini
	-Then wait for bt-agent-mini for successful start (TODO: might be deferred to next step)
	-Init application specific start
	-Wait for both app fully initialized, and bt-agent-mini fully started, then
	-Make device discoverable (hciconfig pscan/iscan/piscan)
*/

/*
hciconfig hci0 up
hciconfig hci0 class 0x0025C0
hciconfig hci0 name Keyboard_model_name
*/

#define HID_PSIZE 10

std::string file_get_contents( const std::string& filename )
{
	std::ifstream in(filename, std::ios::in | std::ios::binary);
	if (!in) return "";
	std::string contents;
	in.seekg(0, std::ios::end);
	contents.resize(in.tellg());
	in.seekg(0, std::ios::beg);
	in.read(&contents[0], contents.size());
	in.close();
	return contents;
}

string hexdump( const void *buf, size_t sz, const string& separ )
{
	static const char hexcars[]="0123456789ABCDEF";
	string resu; unsigned char *bu=(unsigned char *)buf;
	for(size_t i=0;i<sz;i++) { resu+=hexcars[bu[i]/16]; resu+=hexcars[bu[i]%16]; resu+=separ; }
	return resu;
}

void send_hid_packet( int icli, uint8_t packet[10] )
{
	uint8_t bzero[] = { 0xA1, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

	cout << "packet[4] = " << int(packet[4]) << " (0x" << std::hex << std::setfill('0') << std::setw(2) << int(packet[4]) << ")" << endl << std::dec;
	
	int rs = send( icli, packet, HID_PSIZE, 0 );
	if (rs != HID_PSIZE) cout << "send() diff size: " << rs << endl;
	rs = send( icli, &bzero, HID_PSIZE, 0 );
	if (rs != HID_PSIZE) cout << "send() diff size: " << rs << endl;
}

void send_hid_key( int icli, uint8_t key )
{
	uint8_t bytes[] = { 0xA1, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
	bytes[4] = key;
	send_hid_packet( icli, bytes );
}

int main()
{
	//string tmp = pipe_command("ps -C bt-agent-mini -o pid=");
	//cout << "bt-agent-mini=" << tmp << endl;

	bt_initsequence("Keyboard model name",0x0025C0);
	
	//tmp = pipe_command("ps -C bt-agent-mini -o pid=");
	//cout << "bt-agent-mini=" << tmp << endl;

	string service_record = file_get_contents("data/sdp_record_keyboard_multi.xml"), svname = "org.thanhle.btkbservice"; 
	//string service_record = file_get_contents("data/sdp_record_keyboard.xml"), svname = "org.thanhle.btkbservice"; // pour l'instant celui-ci marche...
	DBusConnection *dbus_CX = BlueZ_init( svname );

	cout << "Calling RegisterProfile" << endl;
	jdbus_RegisterProfile( dbus_CX, service_record );

	if (debug) cout << "Now enabling bluetooth sockets" << endl;

	int scnt = socket(AF_BLUETOOTH, SOCK_SEQPACKET, BTPROTO_L2CAP),
		sint = socket(AF_BLUETOOTH, SOCK_SEQPACKET, BTPROTO_L2CAP);

	int enable = 1;
	if (setsockopt(scnt, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0) perror("scnt: setsockopt(SO_REUSEADDR) failed");
	if (setsockopt(sint, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0) perror("sint: setsockopt(SO_REUSEADDR) failed");

	// NONBLOCK AS SECURITY FOR select() -> accept() (race condition of connection dropped between)
	fcntl(scnt, F_SETFL, O_NONBLOCK);
	fcntl(sint, F_SETFL, O_NONBLOCK);

	// Nice workaround for https://stackoverflow.com/questions/9751710/c-c-warning-address-of-temporary-with-bdaddr-any-bluetooth-library
	bdaddr_t bdaddr_any =  {{0, 0, 0, 0, 0, 0}};

	struct sockaddr_l2 cnt_addr, int_addr, remc_addr, remi_addr;
    memset(&cnt_addr, 0, sizeof(cnt_addr));
    cnt_addr.l2_family = AF_BLUETOOTH;
    cnt_addr.l2_bdaddr = bdaddr_any;	// would it work if we let it 0?
	memcpy(&int_addr, &cnt_addr, sizeof(cnt_addr));
	memcpy(&remi_addr, &cnt_addr, sizeof(cnt_addr));
	memcpy(&remc_addr, &cnt_addr, sizeof(cnt_addr));
	socklen_t opt = sizeof(sockaddr_l2);

    uint16_t P_CTRL = 17, P_INTR = 19;	// Service ports from SDP record

    cnt_addr.l2_psm = htobs(P_CTRL); int_addr.l2_psm = htobs(P_INTR);

	if (bind(scnt,(struct sockaddr *)&cnt_addr, sizeof(cnt_addr))) perror("cnt: bind failed.");
	if (bind(sint,(struct sockaddr *)&int_addr, sizeof(int_addr))) perror("int: bind failed.");

	// Some other code put the backlog to 1
	int bklog = 5;
	if (listen(scnt,bklog)) perror("cnt: listen failed"); if (listen(sint,bklog)) perror("int: listen failed");

	
	int ccli=0,icli=0;	//TODO: define exact behaviour with multiple connections.
	/* proposals:
	For HID or RFcomm device: backlog = 2, new connection kills previous one (optionally only from already paired peers) (optionally only if >IDLE sec)
	For Audio sink: backlog = ~10, remember each paired device. Then choose to either allow mixing, or one at a time output.
		Ideally we should use a bluetooth "ping" (or is there an integrated function to detect connection closed/dropped)
	*/
	
	//tmp = pipe_command("ps -C bt-agent-mini -o pid=");
	//cout << "bt-agent-mini=" << tmp << endl;

	fd_set set;
	struct timeval timeout;

	// Now we add control and HID sockets
	const char *btkctl_sockname = "/run/jmd-btk-demo.sock";
	int fd_ctl_in = udgsock_create(btkctl_sockname);

	int base_nfds = max(scnt,sint);
	base_nfds = max(base_nfds,fd_ctl_in);
	//++nfds;
	
	bt_discoverable(true);
	
	//TODO: Add command from console input keys

	cout << "Entering main loop." << endl;

	bool stop_asked = false, disconnect_client = false;
	char command = 0;
	
	int cclimax = 5, cclicount = 0;
	char cpeer[20]={0},ipeer[20]={0};

	while (toupper(command)!='Q')
	{
		command = 0;
		FD_ZERO(&set);
		//TODO: add descriptors to ccli and icli? (detect disconnect?)
		FD_SET(scnt, &set); FD_SET(sint, &set); FD_SET(fd_ctl_in, &set);
		
		int nfds = base_nfds;
		
		// Client control channel is called at client disconnection... seems also at (some kinds of) reconnect
		//if (cclicount < cclimax)
		if (ccli) { FD_SET(ccli, &set); nfds=max(nfds,ccli); }
		
		// Client interrupt channel seems to always trigger continuously...
		//if (icli) { FD_SET(icli, &set); nfds=max(nfds,icli); }
		//TODO: still try to read it to see if something meaningful
		
		++nfds;
		
		timeout.tv_sec = 10; timeout.tv_usec = 0;
		int rv = select(nfds, &set, NULL, NULL, &timeout);
		if (rv) cout << "select() = " << rv << endl;
		if (rv > 0)
		{
			if (FD_ISSET(scnt,&set))
			{
				if (debug) cout << "Control channel awakened" << endl;
				opt=sizeof(sockaddr_l2);
				ccli = accept(scnt, (struct sockaddr *)&remc_addr, &opt);
				if (ccli) 
				{
					ba2str( &remc_addr.l2_bdaddr, cpeer );
					cout << "Connection on control channel from " << cpeer << endl;
				}
				bt_discoverable(false);
			}
			if (FD_ISSET(sint,&set))
			{
				if (debug) cout << "Interrupt channel awakened" << endl;
				opt=sizeof(sockaddr_l2);
				icli = accept(sint, (struct sockaddr *)&remi_addr, &opt);
				if (icli)
				{
					ba2str( &remc_addr.l2_bdaddr, ipeer );
					cout << "Connection on interrupt channel from " << ipeer << endl;
				}
			}
			if (FD_ISSET(fd_ctl_in,&set))
			{
				if (debug) cout << btkctl_sockname << " called" << endl;
				char buf[1024];
				int lg = recvfrom(fd_ctl_in, buf, sizeof(buf), 0, NULL, NULL);
				if (lg<=0) continue;
				command = buf[0];
			}
			if (FD_ISSET(ccli,&set))
			{
				//++cclicount;
				if (debug) cout << "Client control channel awakened" << endl;
				char buf[30]={0};
				ssize_t cliret = recv(ccli,buf,29,MSG_PEEK);
				cout << "recv = " << cliret << endl;
				// According to https://stackoverflow.com/questions/16582303/how-to-check-tcp-peer-is-closed
				// and hopefully supposing it has the same semantics as TCP sockets,
				// we assume: 1 = connection still alive, 0 = connection closed cleanly, negative value = error, connection closed uncleanly
				
				if (cliret == 0)
				{
					cout << cpeer << " disconnected cleanly" << endl;
					disconnect_client = true;
				}
				else if (cliret < 0)
				{
					cout << cpeer << " control channel returns error " << cliret << endl;
					disconnect_client = true;
				}
				else
				{
					cliret = recv(ccli,buf,29,0);
					cout << cpeer << " control channel contains data: " << hexdump(buf,cliret," ") << (cliret>=29?"...":"") << endl;
				}
				// Maybe this shouldn't be done in the latter case? => YUP, seems so, see OBSERVATIONS
				
			}
			//if (FD_ISSET(icli,&set)) cout << "Client interrupt channel awakened" << endl;
		}

		switch (command)
		{
			case 0: break;	// Don't continue; as we can need to disconnect client
			case '+': send_hid_key( icli, 0xe9 ); break;	//0x80 HID code for Volume Up
			case '-': send_hid_key( icli, 0xea ); break;	//0x81 HID code for Volume Down
			case 'a': case 'A': {
				cout << "Sending test string abc" << endl;
				// Example to send keys for an AZERTY keyboard
				//uint8_t bytes[] = { 0xA1, 0x01, 0x00, 0x00, 0x14, 0x05, 0x06, 0x00 }; // NOK => the packet must really be 10 bytes total,
					// even if using send() directly (limitation is not due to function send_hid_packet)
				uint8_t bytes[] = { 0xA1, 0x01, 0x00, 0x00, 0x14, 0x05, 0x06, 0x00, 0x00, 0x00 };	// OK
				send_hid_packet( icli, bytes );
				break; }
			case 'b': send_hid_key( icli, 0x05 ); break;
			case 'P': case 'Q': case 'q': disconnect_client=true; break;
			default:
				cout << "unknown command " << command << " (0x" << std::hex << std::setfill('0') << std::setw(2) << uint8_t(command) << ")" << endl << std::dec;
				break;
		}

		if (disconnect_client)
		{
			cout << "disconnecting/cleaning " << cpeer << endl;
			if (icli) close(icli);
			if (ccli) close(ccli);
			icli = 0; ccli = 0;
			bt_discoverable(true);
			disconnect_client = false;
		}

	}

	close(fd_ctl_in);
	unlink(btkctl_sockname);
	
	return 0;
}
