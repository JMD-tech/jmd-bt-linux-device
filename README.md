## jmd-bt-linux-device

#### Warning: still in early development, expect the API to undergo large changes.  
#### Warning2: obviously not yet documented enough for easy use.

A library to help create Linux-based bluetooth devices, by integrating the many complex steps of bluetooth device-side operation:

* Runtime configuration (hciconfig/bluetoothctl) of Linux's kernel Bluetooth stack: BlueZ
* DBus communication with BlueZ
* Interactions with pairing agent [bt-agent-mini](https://gitlab.com/JMD-tech/bt-agent-mini)
* Generation of SDP records (planned)
* Generation of HID report descriptors (planned)


For the time being (and planned short-term), it supports/will support 3 categories of devices:
* SPP (Serial port), for various sensors/actuators, home automation, remote terminal units, loggers...
* HID, for: obviously keyboards, mices, joysticks and multimedia remotes, but also planned to support more complex, exotic or specialized configurations like: flight simulator instruments panel, mixing table, organ console...
* A2DP (audio sink/receiver), for RPi-based networked audio-amplifier with a Bluetooth input capability.


### Changes to do to the target system before use:  

#### DBus permissions
Place a copy of jmd-demo.conf (or similar adapted to your particular application) into /etc/dbus-1/system.d/ to allow communication with BlueZ over DBus.  

#### Raspberry Pi specific fixes

bthelper (started as `bthelper@hci0.service` by systemd) is a specific script for Raspberry Pi that initializes bluetooth device hardware parameters.  
The systemd .service file starting it has a bug, that can make dependent services start before it has finished its configuration, leading to conflicts.

To solve this:
- Edit file `/usr/lib/systemd/system/bthelper@.service`:  
- change line `Type=simple` to `Type=oneshot`  
- add line `RemainAfterExit=yes` if isn't already here.

That should give for `[Service]` section:

	[Service]
	#Type=simple
	Type=oneshot
	RemainAfterExit=yes
	ExecStart=/usr/bin/bthelper %I

Then in command line as root:  

	systemctl disable bthelper@hci0
	systemctl daemon-reload
	systemctl enable bthelper@hci0

note: Have to check this, but seems that has been fixed in later versions of RaspberryPi OS.

#### bluetoothd service file fixes

By default, bluetoothd enables many plugins, some of which aren't compatible with device-side operation (or other reasons):  
- hostname: if this one is enabled, bluetoothd will set the name of the bluetooth device to the system hostname (raspberrypi), even if a `Name` parameter is set in `/etc/bluetooth/main.cfg`, simply ignoring it.
- sap: this one is for SIM card communication, and causes error lines in logs, simply disable if you don't intend to implement cellular-network.
- input: this one manages external input HID devices, and conflicts if you intend to use the RPi as a HID device itself.


To disable those 3 plugins, edit file `/usr/lib/systemd/system/bluetooth.service`, 
to add parameter `--noplugin=input,sap,hostname` at the end of the `ExecStart` parameter:

`ExecStart=/usr/lib/bluetooth/bluetoothd --noplugin=input,sap,hostname`

Also, there is another bug in the service file discussed here: https://github.com/systemd/systemd/issues/16742  
Impacts applications using bluetooth that are started as services by systemd, basically the `Requires=` and `After=` get ignored and the service is started before bluetooth service has completed initialization.  
You can workaround by using `WantedBy=bluetooth.target` instead of `WantedBy=multi-user.target` in your .service file, ~~or better, fix the problem by commenting out the ConditionPathIsDirectory line in bluetooth.service~~  
update: this doesn't work, or *sometimes* works by timing luck, (at least for systems having some kind of "bthelper" script like Raspberry Pi (Buster OS)) for some esoteric reason you can't have your application be started after the `bthelper@hci0.service`, with `Requires=` it makes your application fail to start with systemd reporting a dependency problem, and with `After=`, the After directive is simply ignored and your service is started before bthelper has finished.  
So keep using `WantedBy=bluetooth.target` in your .service file.


#### bluetoothd configuration

Now the configuration sequence of bluetooth has been fixed (previous steps, and the WantedBy a bit below), we let bluetoothd of BlueZ handle Name and Class setting of the device:  
Edit `/etc/bluetooth/main.cfg`: in the `[General]` section:  
* Set the Name value (example: `Name = MyDevice`)
* Set the Class value (example: `Class = 0x0025C0` for keyboard).  
Else the Class value will be 0 and most OS/devices won't be able to pair with it.
* By default, bluetoothd is set to negociate both "classic" bluetooth BR/EDR (Basic Rate/Enhanced Data Rate) 
and Low-Energy (BLE).  
For use on a Raspberry Pi (or any other SBC capable of running Linux), anyways the electrical consumption of Bluetooth is negligible compared to the main system, 
so you can disable BLE mode by setting `ControllerMode = bredr`  
Else there will appear a device named "Bluetooth LE Device bxxxxxxxxxxx" (MAC address may vary) on the computer after you pair it, in addition to the device with the proper name of your device.  
Doesn't affect re-pairing but might be confusing to end user.
* By the way, you can also enable `FastConnectable = true`

#### Compile dependencies:  
```apt install build-essential libdbus-1-dev libbluetooth-dev```  

#### Installation instructions (build system only):  
(as it's a static library, not required on target system)  
```make clean && make install```

#### Your application service script
Be sure to add  
`Requires=bluetooth.service hciuart.service bthelper@hci0.service` in your `[Unit]` section  
(note: the `bthelper@hci0.service` is for Raspberry Pi and derivatives only, for other devices, check if you have it)  
And  
`WantedBy=bluetooth.target` in your `[Install]` section.  
(`WantedBy=multi-user.target` seems to start your service before bluetooth initialization is complete, at least seen on a Raspberry Pi)

